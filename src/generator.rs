use crate::transaction::{Mempool, generate_transaction};
use std::sync::{Arc, Mutex};
use std::time;
use std::thread;
use crate::network::message::Message;
use crate::network::server::Handle as ServerHandle;
use crate::crypto::hash::{H160, Hashable};
use crate::blockchain::Blockchain;
use std::cmp::Ordering;
use rand::Rng;
use ring::signature::{Ed25519KeyPair, KeyPair};

pub struct Generator {
    server: ServerHandle,
    mut_mempool: Arc<Mutex<Mempool>>,
    mut_blockchain: Arc<Mutex<Blockchain>>,
    address: H160,
    self_key: Ed25519KeyPair,
}

impl Generator {
    pub fn new(
        server: &ServerHandle, 
        key: Ed25519KeyPair,
        mempool: &Arc<Mutex<Mempool>>,
        blockchain: &Arc<Mutex<Blockchain>>
    ) -> Generator {
        // let local_blockchain = blockchain.lock().unwrap();
        // let self_key = Ed25519KeyPair::clone_from(&local_blockchain.self_key);
        // let address = H160::new(&(self_key.public_key()));
        Generator {
            server: server.clone(),
            mut_mempool: Arc::clone(mempool),
            mut_blockchain: Arc::clone(blockchain),
            address: H160::new(&(key.public_key())),
            self_key: key
        }
    }

    pub fn generator_loop(&self) {
        let mut recipients = Vec::new();
        let blockchain = self.mut_blockchain.lock().unwrap();
        let addresses = blockchain.get_state(blockchain.tip()).get_address();
        for addr in addresses {
            if addr.cmp(&self.address) == Ordering::Equal {
                continue;
            }
            recipients.push(addr);
        }
        drop(blockchain);
        let mut nonce = 1;
        loop {
            let mut mempool = self.mut_mempool.lock().unwrap();
            let mut rng = rand::thread_rng();
            let index:u32 = rng.gen_range(0, recipients.len() as u32);
            // println!("index is {}", index);
            let signed_t = generate_transaction(&self.self_key, self.address, nonce, *recipients.get(index as usize).unwrap());
            mempool.add(&signed_t);
            // println!("size of mempool is {}", mempool.len());
            let mut transaction_vec = Vec::new();
            transaction_vec.push(signed_t.hash());
            let msg: Message = Message::NewTransactionHashes(transaction_vec);
            self.server.broadcast(msg);
            nonce = nonce + 1;
            let interval = time::Duration::from_millis(1000 as u64);
            drop(mempool);
            thread::sleep(interval);
        }
    }

    pub fn start(self) {
        let msg: Message = Message::Address(self.address);
        self.server.broadcast(msg);
        println!("Send addr");
        loop {
            let blockchain = self.mut_blockchain.lock().unwrap();
            let curr_state = blockchain.get_state(blockchain.tip());
            if curr_state.get_len() == 3 {
                for addr in curr_state.get_address() {
                    let msg: Message = Message::Address(addr);
                    self.server.broadcast(msg);
                }
                for addr in curr_state.get_address() {
                    println!("account {:?}, nonce is {}, value is {}", addr, curr_state.get_nonce(addr), curr_state.get_value(addr));
                }
                drop(blockchain);
                break;
            }
        }
        println!("ICO finished, generator start");

        thread::Builder::new()
        .name("generator".to_string())
        .spawn(move || {
            self.generator_loop();
        })
        .unwrap();
    }
}