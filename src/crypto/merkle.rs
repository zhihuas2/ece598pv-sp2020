use super::hash::{Hashable, H256};
use serde::{Serialize, Deserialize};
use ring::digest;

#[derive(Serialize, Deserialize, Debug, Clone, Default, PartialEq, Eq)]
pub struct MerkleNode {
    hash_value: H256,
    children: Vec<MerkleNode>,
}

impl MerkleNode {
    pub fn new(combined: H256) -> Self {
        MerkleNode {
            hash_value: combined,
            children: Vec::new(),
        }
    }

    pub fn empty_new() -> Self {
        let arr1:[u8;32] = [0;32];
        let sha_bytes = digest::digest(&digest::SHA256, &arr1);
        MerkleNode {
            hash_value: sha_bytes.into(),
            children: Vec::new(),
        }
    }

    pub fn combine(s1: &H256, s2: &H256) -> H256 {
        let arr1:[u8;32] = s1.into();
        let arr2:[u8;32] = s2.into();
        let concat = [&arr1[..], &arr2[..]].concat();
        let sha_bytes = digest::digest(&digest::SHA256, &concat);
        sha_bytes.into()
    }

    pub fn new_node(left: &MerkleNode, right: &MerkleNode) -> MerkleNode {
        let concatenated = MerkleNode::combine(&left.hash_value, &right.hash_value);
        let mut child = Vec::new();
        child.push(left.clone());
        child.push(right.clone());
        MerkleNode {
            hash_value: concatenated,
            children: child,
        }
    }
}


/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    root: MerkleNode,
    height: u32,
}

impl MerkleTree {
    pub fn build_until_root(mut nodes: Vec<MerkleNode>) -> MerkleNode {
        if nodes.len() == 1 {
            return nodes.remove(0);
        }
        let mut iter = nodes.iter();
        let mut left = iter.next();
        let mut right = iter.next();
        let mut new_nodes: Vec<MerkleNode> = Vec::new();

        while left.is_some() {
            let left_node = left.unwrap();
            let temp = MerkleNode::new(left_node.hash_value.clone());
            let right_node = right.unwrap_or(&temp);
            let next_node = MerkleNode::new_node(&left_node, &right_node);
            new_nodes.push(next_node);
            left = iter.next();
            right = iter.next();
        }
        MerkleTree::build_until_root(new_nodes)
    }

    pub fn new<T>(data: &[T]) -> Self 
    where T: Hashable, {
        let length = data.len() as f64;
        let mut nodes = Vec::new();
        for element in data {
            nodes.push(MerkleNode::new(element.hash()));
        }
        let height: u32 = length.log(2.0).ceil() as u32;
        let tree_root = MerkleTree::build_until_root(nodes);
        MerkleTree {
            root: tree_root,
            height: height,
        }
    }

    pub fn root(&self) -> H256 {
        self.root.hash_value
    }

    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut res: Vec<H256> = Vec::new();
        let indexu: u32 = index as u32;
        let mut curr_node = self.root.clone();
        let mut curr_level = (self.height - 1) as i32;
        let mut boundary = 2u32.pow(self.height - 1);
        while curr_level >= 0 {
            if indexu < boundary {
                res.push(curr_node.children[1].hash_value);
                curr_node = curr_node.children[0].clone();
                if curr_level > 0 {
                    boundary = boundary - 2u32.pow((curr_level - 1) as u32);
                }
            } else {
                res.push(curr_node.children[0].hash_value);
                curr_node = curr_node.children[1].clone();
                if curr_level > 0 {
                    boundary = boundary + 2u32.pow((curr_level - 1) as u32);
                }
            }
            curr_level = curr_level - 1;
        }
        return res;
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut boundary = index;
    let mut res = datum.clone();
    for p in proof.iter().rev() {
        if boundary % 2 == 0 {
            res = MerkleNode::combine(&res, &p);
        } else {
            res = MerkleNode::combine(&p, &res);
        }
        boundary /= 2;
    }
    let arr1:[u8;32] = res.into();
    let arr2:[u8;32] = root.into();
    for i in 0..32 {
        if arr1[i] != arr2[i] {
            return false;
        }
    }
    return true;
}



#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }

    macro_rules! gen_merkle_tree_assignment2 {
        () => {{
            vec![
                (hex!("0000000000000000000000000000000000000000000000000000000000000011")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000022")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000033")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000044")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000055")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000066")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000077")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000088")).into(),
            ]
        }};
    }

    macro_rules! gen_merkle_tree_assignment2_another {
        () => {{
            vec![
                (hex!("1000000000000000000000000000000000000000000000000000000000000088")).into(),
                (hex!("2000000000000000000000000000000000000000000000000000000000000077")).into(),
                (hex!("3000000000000000000000000000000000000000000000000000000000000066")).into(),
                (hex!("4000000000000000000000000000000000000000000000000000000000000055")).into(),
                (hex!("5000000000000000000000000000000000000000000000000000000000000044")).into(),
                (hex!("6000000000000000000000000000000000000000000000000000000000000033")).into(),
                (hex!("7000000000000000000000000000000000000000000000000000000000000022")).into(),
                (hex!("8000000000000000000000000000000000000000000000000000000000000011")).into(),
            ]
        }};
    }

    #[test]
    fn assignment2_merkle_root() {
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6e18c8441bc8b0d1f0d4dc442c0d82ff2b4f38e2d7ca487c92e6db435d820a10")).into()
        );
    }

    #[test]
    fn assignment2_merkle_verify() {
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        for i in 0.. input_data.len() {
            let proof = merkle_tree.proof(i);
            print!("{}", i);
            assert!(verify(&merkle_tree.root(), &input_data[i].hash(), &proof, i, input_data.len()));
        }
        let input_data_2: Vec<H256> = gen_merkle_tree_assignment2_another!();
        let merkle_tree_2 = MerkleTree::new(&input_data_2);
        assert!(!verify(&merkle_tree.root(), &input_data[0].hash(), &merkle_tree_2.proof(0), 0, input_data.len()));
    }

    #[test]
    fn assignment2_merkle_proof() {
        use std::collections::HashSet;
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(5);
        let proof: HashSet<H256> = proof.into_iter().collect();
        let p: H256 = (hex!("c8c37c89fcc6ee7f5e8237d2b7ed8c17640c154f8d7751c774719b2b82040c76")).into();
        assert!(proof.contains(&p));
        let p: H256 = (hex!("bada70a695501195fb5ad950a5a41c02c0f9c449a918937267710a0425151b77")).into();
        assert!(proof.contains(&p));
        let p: H256 = (hex!("1e28fb71415f259bd4b0b3b98d67a1240b4f3bed5923aa222c5fdbd97c8fb002")).into();
        assert!(proof.contains(&p));
    }
}
