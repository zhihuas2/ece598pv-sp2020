use serde::{Serialize,Deserialize};
use crate::crypto::hash::{H256, Hashable, H160};
use ring::signature::{self, Ed25519KeyPair, Signature, KeyPair};
use ring::digest;
// use crate::crypto::key_pair;
// use rand::Rng;
use std::collections::HashMap;
use std::cmp::Ordering;

#[derive(Clone, Serialize, Deserialize, Debug, Default)]
pub struct Transaction {
    recipient: H160,
    sender: H160,
    value: u32,
    account_nonce: u32
}

impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        let bytes: Vec<u8> = bincode::serialize(&self).unwrap();
        let sha_bytes = digest::digest(&digest::SHA256, &bytes);
        sha_bytes.into()
    }
}

impl Transaction {
    pub fn get_recipient(&self) -> H160 {
        self.recipient
    }
    pub fn get_sender(&self) -> H160 {
        self.sender
    }
    pub fn get_value(&self) -> u32 {
        self.value
    }
    pub fn get_nonce(&self) -> u32 {
        self.account_nonce
    }
}

#[derive(Clone, Serialize, Deserialize, Debug, Default)]
pub struct SignedTransaction {
    transaction: Transaction,
    signature: Vec<u8>,
    public_key: Vec<u8>
}

impl Hashable for SignedTransaction {
    fn hash(&self) -> H256 {
        let bytes: Vec<u8> = bincode::serialize(&self).unwrap();
        let sha_bytes = digest::digest(&digest::SHA256, &bytes);
        sha_bytes.into()
    }
}

impl SignedTransaction {
    pub fn get_signature(&self) -> Vec<u8> {
        self.signature.clone()
    }
    pub fn get_publickey(&self) -> Vec<u8> {
        self.public_key.clone()
    }
    pub fn get_transaction(&self) -> Transaction {
        self.transaction.clone()
    }
}

#[derive(Clone, Serialize, Deserialize, Debug, Default)]
pub struct Mempool {
    transaction_buffer: Vec<SignedTransaction>,
    hash_transaction: HashMap<H256, SignedTransaction>
}

impl Mempool {
    pub fn new() -> Mempool {
        Mempool {
            transaction_buffer: Vec::new(),
            hash_transaction: HashMap::new()
        }
    }

    pub fn add(&mut self, signed_t: &SignedTransaction) {
        self.transaction_buffer.push(signed_t.clone());
        self.hash_transaction.insert(signed_t.hash(), signed_t.clone());
    }

    pub fn len(&self) -> usize {
        self.transaction_buffer.len()
    }

    pub fn contains_hash(&self, signed_t_hash: H256) -> bool {
        self.hash_transaction.contains_key(&signed_t_hash)
    }
 
    pub fn get_transaction(&self, signed_t_hash: H256) -> Vec<SignedTransaction> {
        let mut res = Vec::new();
        match self.hash_transaction.get(&signed_t_hash) {
            Some(transaction) => {
                res.push(transaction.clone());
                return res;
            }
            None => {
                return res;
            }
        }
    }

    pub fn remove_transaction(&mut self, signed_ts: Vec<SignedTransaction>) {
        let mut new_buffer = Vec::new();
        for signed_t in &signed_ts {
            let signed_t_hash = signed_t.hash();
            if self.hash_transaction.contains_key(&signed_t_hash) {
                self.hash_transaction.remove_entry(&signed_t_hash);
            }
        }
        for i in 0..self.transaction_buffer.len() {
            match self.transaction_buffer.get(i) {
                Some(child) => {
                    let mut flag:bool = false;
                    for signed_t in &signed_ts {
                        let signed_t_hash = signed_t.hash();
                        let cmp_result = signed_t_hash.cmp(&child.hash());
                        if cmp_result == Ordering::Equal {
                            flag = true;
                            break;
                        }
                    }
                    if !flag {
                        new_buffer.push(child.clone());
                    }
                }
                None => {
                }   
            }
        }
        self.transaction_buffer = new_buffer;
    }

    pub fn pop_out(&mut self, size_limit: usize) -> Vec<SignedTransaction> {
        let mut pop_block = Vec::new();
        for j in 0..size_limit {
            match self.transaction_buffer.get(0) {
                Some(child) => {
                    // println!("pop_out one");
                    pop_block.push(child.clone());
                    self.hash_transaction.remove(&child.hash());
                    self.transaction_buffer.remove(0);
                }
                None => {
                }
            }
        }
        return pop_block.clone();
    }
}

#[derive(Clone, Serialize, Deserialize, Debug, Default)]
pub struct State {
    //account, (nonce, value)
    state: HashMap<H160, (u32, u32)>
}

impl State {
    pub fn new() -> State {
        State {
            state: HashMap::new()
        }
    }

    pub fn get_nonce(&self, account: H160) -> u32 {
        match self.state.get(&account) {
            Some(pair) => {
                return pair.0;
            }
            None => {
                return 0;
            }
        }
    }

    pub fn get_value(&self, account: H160) -> u32 {
        match self.state.get(&account) {
            Some(pair) => {
                return pair.1;
            }
            None => {
                return 0;
            }
        }
    }

    pub fn put_entry(&mut self, account: H160, pair: (u32, u32)) {
        self.state.insert(account, pair);
    }

    pub fn get_len(&self) -> usize {
        self.state.len()
    }

    pub fn get_address(&self) -> Vec<H160> {
        let mut res = Vec::new();
        for key in self.state.keys() {
            res.push(*key);
        }
        return res;
    }
}

pub fn generate_transaction(key: &Ed25519KeyPair, new_sender: H160, nonce: u32, new_recipient: H160) -> SignedTransaction {
    // let new_sender = H160::new(&(key.public_key()));
    // let rng = rand::thread_rng();
    let rand_value:u32 = 1;
    let t = Transaction {
        recipient: new_recipient,
        sender: new_sender,
        value: rand_value,
        account_nonce: nonce
    };
    let t_signature = sign(&t, &key);
    let signed_t = SignedTransaction {
        transaction: t,
        signature: t_signature.as_ref().to_vec(),
        public_key: key.public_key().as_ref().to_vec(),
    };
    return signed_t;
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    // const MESSAGE: &[u8] = b"hello, world";
    // let message = String::from(&t.input);
    // let message_bytes = message.into_bytes();
    let serial: Vec<u8> = bincode::serialize(&t).unwrap();
    let sig = key.sign(&serial);
    // let hex_sig = hex::encode(sig);
    // t.signature = hex_sig;
    // let peer_public_key_bytes = key.public_key().as_ref();
    // t.public_key = peer_public_key_bytes.to_vec();
    return sig;
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let peer_public_key_bytes = public_key.as_ref();
    let peer_public_key = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, peer_public_key_bytes);
    let serial: Vec<u8> = bincode::serialize(&t).unwrap();
    let error = peer_public_key.verify(&serial, signature.as_ref());
    return error.is_ok()
}

pub fn verify_withbytes(t: &Transaction, public_key_bytes: Vec<u8>, signature_bytes: Vec<u8>) -> bool{
    let public_key = signature::UnparsedPublicKey::new(&signature::ED25519, public_key_bytes);
    let serial: Vec<u8> = bincode::serialize(&t).unwrap();
    return public_key.verify(serial.as_ref(), &signature_bytes[..]).is_ok();
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        Default::default()
        // // let key = key_pair::random();
        // let transaction = Transaction {
        //     input: String::from("Zhihua Sun"),
        //     output: String::from(""),
        //     // signature: String::from(""),
        // };
        // return transaction;
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }

    #[test]    
    fn assignment2_transaction_1() {    
        let t = generate_random_transaction();    
        let key = key_pair::random();    
        let signature = sign(&t, &key);    
        assert!(verify(&t, &(key.public_key()), &signature));    
    }    
    #[test]    
    fn assignment2_transaction_2() {    
        let t = generate_random_transaction();    
        let key = key_pair::random();    
        let signature = sign(&t, &key);    
        let key_2 = key_pair::random();    
        let t_2 = generate_random_transaction();    
        assert!(!verify(&t_2, &(key.public_key()), &signature));    
        assert!(!verify(&t, &(key_2.public_key()), &signature));    
    }   
}
