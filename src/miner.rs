use crate::network::server::Handle as ServerHandle;

use log::info;

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time::{SystemTime, UNIX_EPOCH};
use std::time;
use std::thread;
use std::sync::{Arc, Mutex};
use crate::blockchain::Blockchain;
use crate::block::{Header, Content, Block};
use rand::Rng;
use crate::crypto::merkle::MerkleNode;
use crate::crypto::hash::Hashable;
use std::cmp::Ordering;
use crate::network::message::Message;
use crate::transaction::{Mempool};

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    mut_blockchain: Arc<Mutex<Blockchain>>,
    mut_mempool: Arc<Mutex<Mempool>> 
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle, 
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<Mempool>>
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        mut_blockchain: Arc::clone(blockchain),
        mut_mempool: Arc::clone(mempool)
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        // let start_time =  SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();
        let mut mine_number = 0;
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // TODO: actual mining
            // println!("start mining");
            let size_limit = 5;
            let mut blockchain = self.mut_blockchain.lock().unwrap();
            // println!("miner blockchain mutex");
            let par = blockchain.tip();
            let mut rng = rand::thread_rng();
            let nonce:u32 = rng.gen();
            // let sys_time = SystemTime::now();
            let ts =  SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();
            let diff = blockchain.get_tip().get_header().get_difficulty();
            let mer_r = MerkleNode::empty_new();
            let temp_header = Header::new(par, nonce, diff, ts, mer_r);
            let hash_value = temp_header.hash();
            let result = hash_value.cmp(&diff);
            if result == Ordering::Less {
                let mut temp_block = Block::new(temp_header, Content::new());
                // println!("mine block stage 1");
                let mut mempool = self.mut_mempool.lock().unwrap();
                // println!("mine block stage 1.5");
                if mempool.len() >= size_limit {
                    // println!("mine block state 2");
                    let pop_out = mempool.pop_out(size_limit);
                    // println!("pop out len is {}", pop_out.len());
                    temp_block.add_content(&mut pop_out.clone());
                    // println!("transactions len is {}", temp_block.get_content().get_vec().len());
                    blockchain.insert(&temp_block);
                    mine_number = mine_number + 1;
                    // let _duration = ((ts - start_time)/1000) as f32;
                    // let _curr_height = blockchain.get_height() as f32;
                    // let speed = curr_height/duration;
                    println!("mine_number is {}", mine_number);
                    // let self_block_hash = blockchain.all_blocks().clone();
                    // println!("{}", self_block_hash.len());
                    // let mut self_block_hash = Vec::new();
                    // self_block_hash.push(temp_block.hash());
                    let msg: Message = Message::NewBlockHashes(blockchain.all_blocks());
                    self.server.broadcast(msg);
                }
            }


            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}
