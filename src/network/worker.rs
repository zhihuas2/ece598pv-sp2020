use super::message::Message;
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crossbeam::channel;
use log::{debug, warn};
use std::sync::{Arc, Mutex};
use std::thread;
use crate::blockchain::Blockchain;
use crate::crypto::hash::{Hashable, H256};
use crate::block::{Block};
use std::cmp::Ordering;
use std::time::{SystemTime, UNIX_EPOCH};
use crate::transaction::{Mempool, verify_withbytes};
// use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};


#[derive(Clone, Debug)]
pub struct Orphanbuffer {
    buffer: Vec<Block>
}

impl Orphanbuffer {
    pub fn new() -> Self {
        let o = Orphanbuffer {
            buffer: Vec::new()
        };
        return o;
    }
    pub fn insert(&mut self, block: Block) {
        println!("length of orphan buffer {}", self.buffer.len());
        for i in 0..self.buffer.len() {
            match self.buffer.get(i) {
                Some(child) => {
                    let cmp_result = child.hash().cmp(&block.hash());
                    if cmp_result == Ordering::Equal {
                        return;
                    }
                }
                None => {
                }   
            }
        }
        self.buffer.push(block.clone());
    }
    pub fn find_children(&self, blocks: Vec<Block>) -> Vec<usize>{
        // let mut children = self.buffer.len();
        let mut res = Vec::new();
        for block in blocks {
            for i in 0..self.buffer.len() {
                match self.buffer.get(i) {
                    Some(child) => {
                        let cmp_result = block.hash().cmp(&child.get_header().get_parent());
                        if cmp_result == Ordering::Equal {
                            // children = i;
                            res.push(i);
                        } 
                    }
                    None => {

                    }
                }
            }
        }
        return res;
    }
    pub fn get_len(&self) -> usize {
        self.buffer.len()
    }
    pub fn pop_out(&mut self, index: Vec<usize>) -> Vec<Block> {
        let mut new_buffer = Vec::new();
        let mut pop_block = Vec::new();

        for j in &index {
            match self.buffer.get(*j) {
                Some(child) => {
                    pop_block.push(child.clone());
                }
                None => {
                }   
            }
        }
        for i in 0..self.buffer.len() {
            match self.buffer.get(i) {
                Some(child) => {
                    let mut flag = false;
                    for j in &index {
                        if i == *j  {
                            flag = true;
                        }
                    }
                    if !flag {
                        new_buffer.push(child.clone());
                    }
                }
                None => {
                }   
            }
        }
        self.buffer = new_buffer;
        return pop_block;
    }
}
#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    mut_blockchain: Arc<Mutex<Blockchain>>,
    mut_mempool: Arc<Mutex<Mempool>>
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<Mempool>>
) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        mut_blockchain: Arc::clone(blockchain),
        mut_mempool: Arc::clone(mempool)
    }
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&self) {
        let mut orphan_buffer = Orphanbuffer::new();
        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            match msg {
                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }
                Message::Address(account) => {
                    // println!("receive Addr");
                    let mut blockchain = self.mut_blockchain.lock().unwrap();
                    let tip = blockchain.tip();
                    let old_state = blockchain.get_state(tip);
                    let mut new_state = old_state.clone();
                    new_state.put_entry(account, (0, 10000));
                    // println!("current length of state is {}", new_state.get_len());
                    blockchain.put_state(tip, new_state);
                }
                Message::NewBlockHashes(block_hash) => {
                    // let mut_blockchain = self.mut_blockchain.clone();
                    // println!("receive broadcast");
                    // thread::spawn(move || {
                        let blockchain = self.mut_blockchain.lock().unwrap();
                        let self_block_hash = blockchain.all_blocks();
                        let mut not_exist_hash:Vec<H256> = Vec::new();
                        for block in &block_hash {
                            let mut flag = false;
                            if self_block_hash.len() > 0 {
                                for self_block in &self_block_hash {
                                    if block.cmp(&self_block) == std::cmp::Ordering::Equal {
                                        flag = true;
                                        // break;
                                    }
                                }
                            }
                            if flag == false {
                                not_exist_hash.push(*block);
                            }
                        }
                        if not_exist_hash.len() > 0 {
                            peer.write(Message::GetBlocks(not_exist_hash));
                        }
                        // println!("receive broadcast!!!!!")
                    // });
                }
                Message::GetBlocks(not_exist_hash) => {
                    // let mut_blockchain = self.mut_blockchain.clone();
                    // thread::spawn(move || {
                        // println!("receive getBlocks");
                        let blockchain = self.mut_blockchain.lock().unwrap();
                        let blocks = blockchain.get_block(not_exist_hash);
                        // println!("not_exist_hash {}", blocks.len());
                        if blocks.len() > 0 {
                            peer.write(Message::Blocks(blocks));
                        }
                        // println!("receive getBlocks!!!!!!");
                    // });
                }
                Message::Blocks(blocks) => {
                    // let mut_blockchain = self.mut_blockchain.clone();
                    // thread::spawn(move || {
                        // println!("receive Blocks");
                        let mut blockchain = self.mut_blockchain.lock().unwrap();
                        // println!("blocks size {}", blocks.len());
                        let mut new_hash:Vec<H256> = Vec::new();
                        let mut not_exist_hash:Vec<H256> = Vec::new();
                        for block in blocks {
                            let block_h256 = block.hash();
                            let insert_result = blockchain.insert(&block);
                            if insert_result == 3 {
                                //No parent
                                not_exist_hash.push(block.get_header().get_parent());
                                orphan_buffer.insert(block);
                            } else if insert_result == 2 {
                                continue;
                            } else {
                                //insert success
                                let mut mempool = self.mut_mempool.lock().unwrap();
                                mempool.remove_transaction(block.get_content().get_vec());
                                new_hash.push(block_h256);
                                println!("Insert Blocks Success!!!!!! {}", SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis() - block.get_header().get_timestamp());
                                let mut loop_block = Vec::new();
                                loop_block.push(block.clone());
                                loop {
                                    let children_index = orphan_buffer.find_children(loop_block);
                                    if children_index.len() == 0 {
                                        break;
                                    } else {
                                        let children = orphan_buffer.pop_out(children_index);
                                        for child in &children {
                                            let insert_result = blockchain.insert(&child);
                                            mempool.remove_transaction(child.get_content().get_vec());
                                            println!("get one from orphanbuffer!!!!!!! {}", SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis() - block.get_header().get_timestamp());
                                        }
                                        loop_block = children;
                                    }
                                }
                            }
                        }
                        if new_hash.len() > 0 {
                            self.server.broadcast(Message::NewBlockHashes(new_hash));
                        }
                        if not_exist_hash.len() > 0 {
                            peer.write(Message::GetBlocks(not_exist_hash));
                        }
                        // println!("receive Blocks!!!!!!!");
                    // });                
                }
                Message::NewTransactionHashes(transaction_hash) => {
                    // println!("receive NewTransactionHashes");
                    let mempool = self.mut_mempool.lock().unwrap();
                    let transaction = transaction_hash.get(0).unwrap();
                    if !mempool.contains_hash(transaction.clone()) {
                        let mut not_exist_hash = Vec::new();
                        not_exist_hash.push(*transaction);
                        peer.write(Message::GetTransactions(not_exist_hash));
                    }
                }
                Message::GetTransactions(not_exist_hash) => {
                    // println!("receive GetTransactions");
                    let mempool = self.mut_mempool.lock().unwrap();
                    let transaction = not_exist_hash.get(0).unwrap();
                    let transactions = mempool.get_transaction(transaction.clone());
                    if transactions.len() > 0 {
                        peer.write(Message::Transactions(transactions));
                    }
                }
                Message::Transactions(transactions) => {
                    // println!("receive Transactions");
                    let mut mempool = self.mut_mempool.lock().unwrap();
                    let transaction = transactions.get(0).unwrap();
                    if !mempool.contains_hash(transaction.hash()) {
                        let pub_key = transaction.get_publickey();
                        let t = transaction.get_transaction();
                        let signature_bytes = transaction.get_signature();
                        if verify_withbytes(&t, pub_key, signature_bytes) {
                            mempool.add(transaction);                            
                        }
                    }
                }
            }
        }
    }
}
