use serde::{Serialize, Deserialize};
use crate::crypto::hash::{H256, Hashable};
use crate::crypto::merkle::MerkleNode;
use crate::transaction::SignedTransaction;
use ring::digest;
use rand::Rng;
// use std::cmp::Ordering;

#[derive(Clone, Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Header {
    parent: H256,
    nonce: u32,
    difficulty: H256,
    timestamp: u128,
    merkle_root: MerkleNode,
}

impl Header {
    pub fn new(par: H256, non: u32, diff: H256, ts: u128, mer_root: MerkleNode) -> Self {
        Header {
            parent: par,
            nonce: non,
            difficulty: diff,
            timestamp: ts,
            merkle_root: mer_root,
        }
    }

    pub fn get_parent(&self) -> H256 {
        self.parent
    }

    pub fn get_difficulty(&self) -> H256 {
        self.difficulty
    }

    pub fn get_timestamp(&self) -> u128 {
        self.timestamp
    }

    // fn eq(&self, other: &Self) -> bool {
    //     let parent_cmp_result = self.parent.cmp(&other.get_parent());        
    //     let diff_cmp_result = self.difficulty.cmp(&other.get_difficulty());
    //     if parent_cmp_result == Ordering::Equal && diff_cmp_result == Ordering::Equal 
    //         && self.nonce == other.nonce && self.timestamp == other.get_timestamp() {
    //             return true;
    //         } else {
    //             return false;
    //         }
    // }
}

// impl PartialEq for Header {

// }

// impl Eq for Header {}

impl Hashable for Header {
    fn hash(&self) -> H256 {
        let bytes: Vec<u8> = bincode::serialize(&self).unwrap();
        let sha_bytes = digest::digest(&digest::SHA256, &bytes);
        sha_bytes.into()
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Content{
    content: Vec<SignedTransaction>,
}

impl Content {
    pub fn new() -> Self {
        Content {
            content: Vec::new(),
        }
    }

    pub fn add(&mut self, signed_ts: &mut Vec<SignedTransaction>) {
        for signed_t in signed_ts {
            // println!("add one to content");
            self.content.push(signed_t.clone());
        }
    }

    pub fn get_vec(&self) -> Vec<SignedTransaction> {
        self.content.clone()
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Block {
    header: Header,
    content: Content,
}

impl Block {
    pub fn new(head: Header, cont: Content) -> Self {
        Block {
            header: head,
            content: cont,
        }
    }

    pub fn get_header(&self) -> Header {
        self.header.clone()
    }

    pub fn add_content(&mut self, signed_ts: &mut Vec<SignedTransaction>) {
        self.content.add(signed_ts);
    }

    pub fn get_content(&self) -> Content {
        self.content.clone()
    }
}

impl Hashable for Block {
    fn hash(&self) -> H256 {
        self.header.hash()
    }
}

// impl PartialEq for Block {

// }

// impl Eq for Block {}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;

    pub fn generate_random_block(parent: &H256) -> Block {
        let mut rng = rand::thread_rng();
        let non:u32 = rng.gen();
        let ts:u128 = rng.gen();
        let diffran:u8 = rng.gen();
        let mut diffranv = Vec::new();
        diffranv.push(diffran);
        let sha_bytes = digest::digest(&digest::SHA256, &diffranv);
        let mer_r = MerkleNode::empty_new();
        let h = Header::new(*parent, non, sha_bytes.into(), ts, mer_r);
        Block::new(h, Content::new())
    }
}
