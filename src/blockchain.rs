use crate::block::{Header, Content, Block};
use crate::crypto::hash::{H160, H256};
use crate::crypto::hash::Hashable;
// use ring::digest;
use std::collections::HashMap;
use crate::crypto::merkle::MerkleNode;
use rand::Rng;
use std::cmp::Ordering;
use crate::transaction::{SignedTransaction, State};
use ring::signature::{Ed25519KeyPair, KeyPair};
use crate::transaction::{verify_withbytes};

pub struct Blockchain {
    hash_block: HashMap<H256, Block>,
    hash_height: HashMap<H256, u32>,
    hash_state: HashMap<H256, State>,
    // pub self_key: Ed25519KeyPair,
    tip: Block,
}

impl Blockchain {
    pub fn generate_random_block(parent: &H256) -> Block {
        let non:u32 = 1;
        let ts:u128 = 14;
        let diffran:u8 = 13;
        let mut diffranv = Vec::new();
        diffranv.push(diffran);
        // let sha_bytes = digest::digest(&digest::SHA256, &diffranv);
        let mut sha_bytes:[u8;32] = [0;32];
        sha_bytes[1] = 8;
        let mer_r = MerkleNode::empty_new();
        let h = Header::new(*parent, non, sha_bytes.into(), ts, mer_r);
        Block::new(h, Content::new())
    }

    /// Create a new blockchain, only containing the genesis block
    pub fn new(public_key: <Ed25519KeyPair as KeyPair>::PublicKey) -> Self {
        let sha_bytes:[u8;32] = [0;32];
        let h256 = sha_bytes.into();
        let genesis = Blockchain::generate_random_block(&h256);
        let hash_b = HashMap::new();
        let hash_h = HashMap::new();
        let hash_s = HashMap::new();
        let address = H160::new(&public_key.clone());
        let mut b = Blockchain {
            hash_height: hash_h,
            hash_block: hash_b,
            hash_state: hash_s,
            // self_key: key,
            tip: genesis.clone(),
        };
        let mut s = State::new();
        s.put_entry(address, (0, 10000));
        b.hash_block.insert(genesis.hash(), genesis.clone());
        b.hash_height.insert(genesis.hash(), 0);
        b.hash_state.insert(genesis.hash(), s);
        return b;
    }
    
    pub fn generate_newstate (parent_state: State, signed_ts: Vec<SignedTransaction>) -> State {
        let mut new_state = parent_state.clone();
        for signed_t in signed_ts {
            let t = signed_t.get_transaction();
            let sender = t.get_sender();
            let recipient = t.get_recipient();
            let value = t.get_value();
            let account_nonce = t.get_nonce();
            let old_sender_value = new_state.get_value(sender);
            let old_recipient_value = new_state.get_value(recipient);
            let old_recipient_nonce = new_state.get_nonce(recipient);
            // println!("value is {}, account_nonce is {}", value, account_nonce);
            new_state.put_entry(sender, (account_nonce, old_sender_value - value));
            new_state.put_entry(recipient, (old_recipient_nonce, old_recipient_value + value));
        }
        return new_state;
    }

    pub fn check(&self, block: &Block, parent_state: &State) -> bool {
        let content = block.get_content();
        let mut is_valid = true;
        for signed_t in content.get_vec() {
            let pub_key = signed_t.get_publickey();
            let t = signed_t.get_transaction();
            let signature_bytes = signed_t.get_signature();

            let sender = t.get_sender();
            // let recipient = t.get_recipient();
            let value = t.get_value();
            // let account_nonce = t.get_nonce();
            let old_sender_value = parent_state.get_value(sender);
            // let old_recipient_value = parent_state.get_value(recipient);
            // let old_sender_nonce = parent_state.get_nonce(sender);
            // let old_recipient_nonce = parent_state.get_nonce(recipient);
            
            if !verify_withbytes(&t, pub_key, signature_bytes) || old_sender_value < value {
            // if !verify_withbytes(&t, pub_key, signature_bytes) {
                is_valid = false;
                break;
            }
        }
        return is_valid;
    }


    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block) -> u32{
        // println!("insert one");
        let tip_h256 = self.tip();
        let block_h256 = block.hash();
        if self.hash_block.contains_key(&block_h256) {
            return 2;
        }
        match self.hash_height.get(&block.get_header().get_parent()) {
            Some(parent_hash_height) => {
                match self.hash_block.get(&block.get_header().get_parent()) {
                    Some(parent_block) => {
                        match self.hash_state.get(&block.get_header().get_parent()) {
                            Some(parent_state) => {
                                if !Blockchain::check(self, block, parent_state) {
                                    return 2;
                                }
                                let parent_diff = &parent_block.get_header().get_difficulty();
                                let diff_euqal_result = block.get_header().get_difficulty().cmp(parent_diff);
                                let pow_result = block.hash().cmp(parent_diff);
                                if diff_euqal_result == Ordering::Equal && pow_result == Ordering::Less {
                                    let new_state = Blockchain::generate_newstate(parent_state.clone(), block.get_content().get_vec());
                                    let addrs = new_state.get_address();
                                    for addr in addrs {
                                        println!("account {:?}: nonce is {}, value is {}", addr, new_state.get_nonce(addr), new_state.get_value(addr));
                                    }
                                    let curr_height = parent_hash_height + 1;
                                    self.hash_height.insert(block_h256, curr_height);
                                    self.hash_block.insert(block_h256, block.clone());
                                    self.hash_state.insert(block_h256, new_state);
                                    if curr_height > *self.hash_height.get(&tip_h256).unwrap() {
                                        self.tip = block.clone();
                                    }
                                    println!("current height is {} {}", curr_height, block_h256);
                                    // println!("mine_number is {}", mine_number);
                                    return 1;
                                } else {
                                    return 2;
                                }
                            }
                            None => {
                                return 3;
                            }
                        }
                    }
                    None => {
                        return 3;
                    }
                }   
            },
            None => {
                return 3;
            }
        }
        // println!("insert one!!!!!!");
    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        self.tip.hash()
    }

    pub fn get_tip(&self) -> Block {
        self.tip.clone()
    }

    pub fn get_height(&self) -> u32 {
        *self.hash_height.get(&self.tip()).unwrap()
    }

    pub fn put_state(&mut self, block: H256, new_state: State) {
        self.hash_state.insert(block, new_state);
    }

    pub fn get_state(&self, block: H256) -> State {
        self.hash_state.get(&block).unwrap().clone()
    }

    // pub fn get_key(&self) -> Ed25519KeyPair {
    //     self.self_key
    // }

    pub fn get_block(&self, keyset: Vec<H256>) -> Vec<Block> {
        let mut res:Vec<Block> = Vec::new();
        for key in keyset {
            match self.hash_block.get(&key) {
                Some(block) => {
                    res.push(block.clone());
                }
                None => {}
            }
        }
        return res;
    }

    pub fn all_blocks(&self) -> Vec<H256> {
        let mut res = Vec::new();
        let mut height = *self.hash_height.get(&self.tip()).unwrap() as i32;
        let mut curr_node = self.tip();
        while height >= 0 {
            res.push(curr_node);
            curr_node = self.hash_block.get(&curr_node).unwrap().get_header().get_parent();
            height = height - 1;
        }
        let mut res_rev = Vec::new();
        while let Some(top) = res.pop() {
            res_rev.push(top);
        }
        return res_rev;
    }

    /// Get the last block's hash of the longest chain
    #[cfg(any(test, test_utilities))]
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        let mut res = Vec::new();
        let mut height = *self.hash_height.get(&self.tip()).unwrap() as i32;
        let mut curr_node = self.tip();
        while height >= 0 {
            res.push(curr_node);
            curr_node = self.hash_block.get(&curr_node).unwrap().get_header().get_parent();
            height = height - 1;
        }
        return res;
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;
    use crate::crypto::key_pair;

    #[test]
    fn insert_one() {
        let key = key_pair::random();
        let mut blockchain = Blockchain::new(key.public_key().clone());
        let genesis_hash = blockchain.tip();
        let block0 = generate_random_block(&genesis_hash);

        blockchain.insert(&block0);
        // assert_eq!(blockchain.tip(), block0.hash());

        let block1 = generate_random_block(&block0.hash());
        blockchain.insert(&block1);

        let block2 = generate_random_block(&block0.hash());
        blockchain.insert(&block2);

        let block3 = generate_random_block(&block2.hash());
        blockchain.insert(&block3);

        let temp = blockchain.all_blocks_in_longest_chain();
        assert_eq!(genesis_hash, *temp.get(3).unwrap());
    }
}
